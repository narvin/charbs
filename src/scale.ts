import * as d3 from "d3"

export type Scale =
  | d3.ScaleLinear<number, number>
  | d3.ScaleTime<number, number>

export type ScaleStrategy<TDatum, TScale extends Scale> = (
  data: TDatum[],
  range: [number, number],
) => TScale

function getDateInfo({
  interval,
  prev,
  curr,
  next,
}: {
  interval: d3.CountableTimeInterval
  prev: Date
  curr: Date
  next: Date
}): { prev: Date; curr: Date; next: Date; type: "left" | "inner" | "right" } {
  if (+interval(curr) !== +interval.offset(next, -1)) {
    return { prev, curr, next, type: "right" }
  }
  if (+interval(prev) !== +interval.offset(curr, -1)) {
    return { prev, curr, next, type: "left" }
  }
  return { prev, curr, next, type: "inner" }
}

export function packedDateScale({
  dates,
  interval,
  range,
  utc = true,
}: {
  dates: Date[]
  interval: d3.CountableTimeInterval
  range: number[]
  utc?: boolean
}): d3.ScaleTime<number, number, never> {
  if (dates.length === 0) {
    return d3.scaleUtc(range)
  }
  const paddedDates = [dates[0], ...dates, ...dates.slice(-1)]
  const triads = dates.map((_, idx) => ({
    idx,
    ...getDateInfo({
      interval,
      prev: paddedDates[idx],
      curr: paddedDates[idx + 1],
      next: paddedDates[idx + 2],
    }),
  }))
  const triadBands = triads.filter(({ type }) => type !== "inner")
  const domainBands = triadBands
    .reduce<
      {
        idx: number
        curr: Date
        isCollapsed: boolean
      }[]
    >(
      (acc, curr) => [
        ...acc,
        { idx: curr.idx, curr: curr.curr, isCollapsed: false },
        ...(curr.type === "right"
          ? [
              {
                idx: curr.idx + 1,
                curr: interval.offset(curr.curr, 1),
                isCollapsed: true,
              },
            ]
          : []),
      ],
      [],
    )
    .slice(0, -1)
  const collapsedBands = domainBands.reduce<[Date, Date][]>(
    (acc, curr, idx) =>
      curr.isCollapsed
        ? [...acc, [curr.curr, domainBands[idx + 1].curr]]
        : [...acc],
    [],
  )
  const rangeStep = (range[1] - range[0]) / (dates.length - 1)
  const rangeBands = domainBands.map(({ idx }) => rangeStep * idx)
  const scaleFn = utc ? d3.scaleUtc : d3.scaleTime
  const scale = scaleFn(
    domainBands.map(({ curr }) => curr),
    rangeBands,
  )
  const ogTicks = scale.ticks.bind(scale)
  scale.ticks = function customTicks(count) {
    if (
      count === undefined &&
      (interval === d3.timeDay || interval === d3.utcDay)
    ) {
      const ticks = ogTicks().map((tick) =>
        tick.getDay() === 0 ? interval.offset(tick, 1) : tick,
      )
      return ticks.filter((tick) =>
        collapsedBands.every(([start, end]) => !(tick >= start && tick < end)),
      )
    }
    if (count === undefined || typeof count === "number") {
      return ogTicks(count).filter((tick) =>
        collapsedBands.every(([start, end]) => !(tick >= start && tick < end)),
      )
    }
    return ogTicks(count)
  }
  return scale
}
