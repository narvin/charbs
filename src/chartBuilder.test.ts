import { Mock, beforeEach, describe, expect, test, vi } from "vitest"
import * as d3 from "d3"
import * as chart from "./chart"
import * as chartBuilder from "./chartBuilder"
import * as scale from "./scale"

const d3Mock = vi.hoisted(function magicMock() {
  const mock = vi.fn()
  mock.mockReturnValue(mock)
  const proxy: Mock = new Proxy(mock, {
    get(target, prop) {
      if (prop === "mock") return target.mock
      if (prop === "mockClear") return target.mockClear
      return proxy
    },
    apply(target, _thisArg, args) {
      target(...args)
      return proxy
    },
  })
  return proxy
})

vi.mock("d3", async function mod() {
  const actual = await vi.importActual<typeof import("d3")>("d3")
  return {
    ...actual,
    select: d3Mock,
  }
})

function enterStrategyFactory<
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
>({
  hScale: _hScale,
  vScale: _vScale,
}: chart.EnterStrategyFactoryProps<
  THScale,
  TVScale
>): chart.EnterStrategy<unknown> {
  return function strategy(parent) {
    return parent.append("g")
  }
}

describe("chartBuilder", () => {
  beforeEach(() => {
    d3Mock.mockClear()
  })
  test("uses default dimension", () => {
    chartBuilder
      .chartBuilder()
      .title("foo")
      .parentSelector("#app")
      .hScaleStrategy((_values, _range) => d3.scaleUtc())
      .vScaleStrategy((_values, _range) => d3.scaleLinear())
      .enterStrategyFactory(enterStrategyFactory)
      .data([])
      .build()
    expect(d3Mock.mock.calls.slice(0, 4)).toEqual([
      ["#app"],
      ["svg"],
      ["width", 800],
      ["height", 600],
    ])
  })
  test("uses passed dimension width", () => {
    chartBuilder
      .chartBuilder()
      .title("foo")
      .parentSelector("#app")
      .hScaleStrategy((_values, _range) => d3.scaleUtc())
      .vScaleStrategy((_values, _range) => d3.scaleLinear())
      .enterStrategyFactory(enterStrategyFactory)
      .data([])
      .dimension({ width: 400 })
      .build()
    expect(d3Mock.mock.calls.slice(0, 4)).toEqual([
      ["#app"],
      ["svg"],
      ["width", 400],
      ["height", 600],
    ])
  })
  test("uses passed dimension height", () => {
    chartBuilder
      .chartBuilder()
      .title("foo")
      .parentSelector("#app")
      .hScaleStrategy((_values, _range) => d3.scaleUtc())
      .vScaleStrategy((_values, _range) => d3.scaleLinear())
      .enterStrategyFactory(enterStrategyFactory)
      .data([])
      .dimension({ height: 400 })
      .build()
    expect(d3Mock.mock.calls.slice(0, 4)).toEqual([
      ["#app"],
      ["svg"],
      ["width", 800],
      ["height", 400],
    ])
  })
  test("uses passed dimension", () => {
    chartBuilder
      .chartBuilder()
      .title("foo")
      .parentSelector("#app")
      .hScaleStrategy((_values, _range) => d3.scaleUtc())
      .vScaleStrategy((_values, _range) => d3.scaleLinear())
      .enterStrategyFactory(enterStrategyFactory)
      .data([])
      .dimension({ width: 400, height: 200 })
      .build()
    expect(d3Mock.mock.calls.slice(0, 4)).toEqual([
      ["#app"],
      ["svg"],
      ["width", 400],
      ["height", 200],
    ])
  })
})
