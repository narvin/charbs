import "./style.css"
import * as d3 from "d3"
import * as candlestick from "./candlestick"
import * as chartBuilder from "./chartBuilder"
import * as sampleData from "./sampleData"

const data = d3
  .csvParse(sampleData.DATA)
  .slice(-60)
  .map(
    ({ Date: date, Open: open, High: high, Low: low, "Adj Close": close }) => ({
      date: new Date(date),
      open: +open,
      close: +close,
      low: +low,
      high: +high,
    }),
  )

chartBuilder
  .chartBuilder<
    candlestick.Candlestick,
    d3.ScaleTime<number, number>,
    d3.ScaleLinear<number, number>
  >()
  .title("My Chart Builder Chart")
  .parentSelector("#app")
  .hScaleStrategy(candlestick.dateScaleStrategy)
  .vScaleStrategy(candlestick.priceScaleStrategy)
  .enterStrategyFactory(candlestick.createCandlestickEnterStrategyFactory())
  .data(data)
  .dimension({ width: 800, height: 600 })
  .margin({ top: 20, right: 20, bottom: 20, left: 20 })
  .axesMargin({ top: 60, right: 40, bottom: 20, left: 10 })
  .bleed(0)
  .build()
