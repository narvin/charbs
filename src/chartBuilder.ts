/* eslint-disable import/prefer-default-export */
import * as chart from "./chart"
import * as scale from "./scale"

export function chartBuilder<
  TDatum,
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
>() {
  function builder(
    props: Partial<chart.ChartProps<TDatum, THScale, TVScale>> = {},
  ) {
    return {
      title(value: string) {
        const { parentSelector } = builder({
          ...props,
          title: value,
        })
        return { parentSelector }
      },
      parentSelector(value: string) {
        const { hScaleStrategy } = builder({
          ...props,
          parentSelector: value,
        })
        return { hScaleStrategy }
      },
      hScaleStrategy(value: scale.ScaleStrategy<TDatum, THScale>) {
        const { vScaleStrategy } = builder({
          ...props,
          hScaleStrategy: value,
        })
        return { vScaleStrategy }
      },
      vScaleStrategy(value: scale.ScaleStrategy<TDatum, TVScale>) {
        const { enterStrategyFactory } = builder({
          ...props,
          vScaleStrategy: value,
        })
        return { enterStrategyFactory }
      },
      enterStrategyFactory(
        value: chart.EnterStrategyFactory<TDatum, THScale, TVScale>,
      ) {
        const { data } = builder({
          ...props,
          enterStrategyFactory: value,
        })
        return { data }
      },
      data(value: TDatum[]) {
        const { build, dimension, margin, axesMargin, bleed } = builder({
          ...props,
          data: value,
        })
        return { build, dimension, margin, axesMargin, bleed }
      },
      dimension(value: chart.Dimension) {
        const { build, margin, axesMargin, bleed } = builder({
          ...props,
          dimension: value,
        })
        return { build, margin, axesMargin, bleed }
      },
      margin(value: chart.Margin) {
        const { build, dimension, axesMargin, bleed } = builder({
          ...props,
          margin: value,
        })
        return { build, dimension, axesMargin, bleed }
      },
      axesMargin(value: chart.Margin) {
        const { build, dimension, margin, bleed } = builder({
          ...props,
          axesMargin: value,
        })
        return { build, dimension, margin, bleed }
      },
      bleed(value: number) {
        const { build, dimension, margin, axesMargin } = builder({
          ...props,
          bleed: value,
        })
        return { build, dimension, margin, axesMargin }
      },
      build() {
        return chart.chart(props as chart.ChartProps<TDatum, THScale, TVScale>)
      },
    }
  }
  const { title } = builder()
  return { title }
}
