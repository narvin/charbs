import * as d3 from "d3"
import * as scale from "./scale"

/**
 * Properties to append a {@link d3.axisBottom} or a {@link d3.axisRight} to a {@link
 * d3.Selection}.
 *
 * The client can consider the selection as having logical inline and block directions. The
 * axis is parallel to the inline direction, and positioned at the end of the block. The
 * margins can be used to shift the axis along either direction.
 */
export type BlockEndAxisProps<TScale extends scale.Scale> = {
  /** The scale used to create the axis. */
  inlineScale: TScale
  /**
   * The size of the selction along the block direction. For a bottom axis, this would be
   * the height, so the axis can be shifted down from the origin.
   */
  blockSize: number
  /**
   * The amound to shift the axis along the block direction. For a bottom axis, this would be
   * the bottom margin.
   */
  marginBlockEnd?: number
  /**
   * The amound to shift the axis along the inline direction. For a bottom axis, this would
   * be the left margin.
   */
  marginInlineStart?: number
}

/** A selection to append an axis to. */
type ParentSelection<TParent extends SVGElement, TDatum> = d3.Selection<
  TParent,
  TDatum,
  HTMLElement,
  unknown
>

/**
 * Create a function that can be called with a {@link d3.Selection} to append a
 * {@link d3.axisBottom} to.
 *
 * @param props - {@link BlockEndAxisProps}
 * @returns A function that will append an axisBottom to a selection. This function can be
 *   used as the argument to {@link d3.Selection.call}.
 */
export function appendBottomAxis<
  TParent extends SVGElement,
  TDatum,
  TScale extends scale.Scale,
>({
  inlineScale,
  blockSize,
  marginBlockEnd = 0,
  marginInlineStart = 0,
}: BlockEndAxisProps<TScale>): (
  parent: ParentSelection<TParent, TDatum>,
) => void {
  return function append(parent: ParentSelection<TParent, TDatum>) {
    parent
      .append("g")
      .attr("class", "chart__axis chart__axis--bottom")
      .attr(
        "transform",
        `translate(${marginInlineStart}, ${blockSize - marginBlockEnd})`,
      )
      .call(d3.axisBottom(inlineScale))
  }
}

/**
 * Create a function that can be called with a {@link d3.Selection} to append a
 * {@link d3.axisRight} to.
 *
 * @param props - {@link BlockEndAxisProps}
 * @returns A function that will append an axisRight to a selection. This function can be used
 *   as the argument to {@link d3.Selection.call}.
 */
export function appendRightAxis<
  TParent extends SVGElement,
  TDatum,
  TScale extends scale.Scale,
>({
  inlineScale,
  blockSize,
  marginBlockEnd = 0,
  marginInlineStart = 0,
}: BlockEndAxisProps<TScale>): (
  parent: ParentSelection<TParent, TDatum>,
) => void {
  return function append(parent: ParentSelection<TParent, TDatum>) {
    parent
      .append("g")
      .attr("class", "chart__axis chart__axis--right")
      .attr(
        "transform",
        `translate(${blockSize - marginBlockEnd}, ${marginInlineStart})`,
      )
      .call(d3.axisRight(inlineScale))
  }
}
