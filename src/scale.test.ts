import { beforeEach, describe, expect, test, vi } from "vitest"
import * as d3 from "d3"
import * as scale from "./scale"

vi.mock("d3", async function mod() {
  const actual = await vi.importActual<typeof import("d3")>("d3")
  return {
    ...actual,
    scaleTime: vi.fn().mockImplementation(() => ({ ticks: vi.fn() })),
    scaleUtc: vi.fn().mockImplementation(() => ({ ticks: vi.fn() })),
  }
})

describe("packedDateScale", () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })
  test("creates a UTC scale by default", () => {
    scale.packedDateScale({
      dates: ["2024-01-01", "2024-01-02"].map((s) => new Date(s)),
      interval: d3.timeDay,
      range: [0, 600],
    })
    expect(d3.scaleTime).not.toHaveBeenCalled()
    expect(d3.scaleUtc).toHaveBeenCalled()
  })
  test("creates a UTC scale when utc is true", () => {
    scale.packedDateScale({
      dates: ["2024-01-01", "2024-01-02"].map((s) => new Date(s)),
      interval: d3.timeDay,
      range: [0, 600],
      utc: true,
    })
    expect(d3.scaleTime).not.toHaveBeenCalled()
    expect(d3.scaleUtc).toHaveBeenCalled()
  })
  test("creates a time scale when utc is false", () => {
    scale.packedDateScale({
      dates: ["2024-01-01", "2024-01-02"].map((s) => new Date(s)),
      interval: d3.timeDay,
      range: [0, 600],
      utc: false,
    })
    expect(d3.scaleTime).toHaveBeenCalled()
    expect(d3.scaleUtc).not.toHaveBeenCalled()
  })
  test("creates default scale when called with 0 dates", () => {
    scale.packedDateScale({
      dates: [],
      interval: d3.timeDay,
      range: [0, 600],
    })
    expect(d3.scaleUtc).toHaveBeenCalledWith([0, 600])
  })
  test("creates a scale when called with 2 adjacent dates", () => {
    const dates = ["2024-01-01", "2024-01-02"].map((s) => new Date(s))
    scale.packedDateScale({
      dates,
      interval: d3.utcDay,
      range: [0, 600],
    })
    expect(d3.scaleUtc).toHaveBeenCalledWith(dates, [0, 600])
  })
  test("creates a scale when called with 2 non-adjacent dates", () => {
    const dates = ["2024-01-01", "2024-01-03"].map((s) => new Date(s))
    const expDates = ["2024-01-01", "2024-01-02", "2024-01-03"].map(
      (s) => new Date(s),
    )
    const range = [0, 600]
    const expRange = [0, 600, 600]
    scale.packedDateScale({
      dates,
      interval: d3.utcDay,
      range,
    })
    expect(d3.scaleUtc).toHaveBeenCalledWith(expDates, expRange)
  })
  test("creates a scale when called with 3 adjacent dates", () => {
    const dates = ["2024-01-01", "2024-01-02", "2024-01-03"].map(
      (s) => new Date(s),
    )
    scale.packedDateScale({
      dates,
      interval: d3.utcDay,
      range: [0, 600],
    })
    expect(d3.scaleUtc).toHaveBeenCalledWith(
      ["2024-01-01T00:00:00.000Z", "2024-01-03T00:00:00.000Z"].map(
        (s) => new Date(s),
      ),
      [0, 600],
    )
  })
  test("creates a scale when called with 3 non-adjacent dates", () => {
    const dates = ["2024-01-01", "2024-01-03", "2024-01-04"].map(
      (s) => new Date(s),
    )
    const expDates = [
      "2024-01-01",
      "2024-01-02",
      "2024-01-03",
      "2024-01-04",
    ].map((s) => new Date(s))
    const range = [0, 600]
    const expRange = [0, 300, 300, 600]
    scale.packedDateScale({
      dates,
      interval: d3.utcDay,
      range,
    })
    expect(d3.scaleUtc).toHaveBeenCalledWith(expDates, expRange)
  })
  test("creates a UTC scale with domain and range bands", () => {
    const dates = [
      "2024-01-01",
      "2024-01-02",
      "2024-01-03",
      "2024-01-06",
      "2024-01-07",
      "2024-01-08",
      "2024-01-09",
      "2024-01-10",
      "2024-01-13",
      "2024-01-14",
      "2024-01-15",
      "2024-01-16",
      "2024-01-17",
    ].map((s) => new Date(s))
    const expDates = [
      "2024-01-01T00:00:00.000Z",
      "2024-01-03T00:00:00.000Z",
      "2024-01-04T00:00:00.000Z",
      "2024-01-06T00:00:00.000Z",
      "2024-01-10T00:00:00.000Z",
      "2024-01-11T00:00:00.000Z",
      "2024-01-13T00:00:00.000Z",
      "2024-01-17T00:00:00.000Z",
    ].map((s) => new Date(s))
    const range = [0, 600]
    const expRange = [0, 100, 150, 150, 350, 400, 400, 600]
    scale.packedDateScale({
      dates,
      interval: d3.utcDay,
      range,
    })
    expect(d3.scaleUtc).toHaveBeenCalledWith(expDates, expRange)
  })
  test("creates a time scale with domain and range bands", () => {
    const dates = [
      "2024-01-01 EST",
      "2024-01-02 EST",
      "2024-01-03 EST",
      "2024-01-06 EST",
      "2024-01-07 EST",
      "2024-01-08 EST",
      "2024-01-09 EST",
      "2024-01-10 EST",
      "2024-01-13 EST",
      "2024-01-14 EST",
      "2024-01-15 EST",
      "2024-01-16 EST",
      "2024-01-17 EST",
    ].map((s) => new Date(s))
    const expDates = [
      "2024-01-01T05:00:00.000Z",
      "2024-01-03T05:00:00.000Z",
      "2024-01-04T05:00:00.000Z",
      "2024-01-06T05:00:00.000Z",
      "2024-01-10T05:00:00.000Z",
      "2024-01-11T05:00:00.000Z",
      "2024-01-13T05:00:00.000Z",
      "2024-01-17T05:00:00.000Z",
    ].map((s) => new Date(s))
    const range = [0, 600]
    const expRange = [0, 100, 150, 150, 350, 400, 400, 600]
    scale.packedDateScale({
      dates,
      interval: d3.timeDay,
      range,
    })
    expect(d3.scaleUtc).toHaveBeenCalledWith(expDates, expRange)
  })
})
