import * as d3 from "d3"
import * as axis from "./axis"
import * as scale from "./scale"

/**
 * Properties to create a {@link d3.Selection.join} enter function.
 *
 * The client can return a function with a closure that contains the given scales for positioning
 * elements.
 */
export type EnterStrategyFactoryProps<
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
> = {
  /** The horizontal scale to be used for positioning elements. */
  hScale: THScale
  /** The vertical scale to be used for positioning elements. */
  vScale: TVScale
}

/**
 * A {@link d3.Selection.join} enter function.
 *
 * @param enter - The enter selection.
 * @returns - The appended selection.
 */
export type EnterStrategy<TDatum> = (
  enter: d3.Selection<d3.EnterElement, TDatum, SVGGElement, TDatum>,
) => d3.Selection<SVGGElement, TDatum, SVGGElement, TDatum>

/**
 * A function that creates an {@link EnterStrategy} function.
 *
 * The returned function should have a closure that contains the scales for positioning elements.
 *
 * @param props - {@link EnterStrategyFactoryProps}
 * @returns - A function that will receive the enter selection.
 */
export type EnterStrategyFactory<
  TDatum,
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
> = ({
  hScale,
  vScale,
}: EnterStrategyFactoryProps<THScale, TVScale>) => EnterStrategy<TDatum>

export type Dimension = {
  width?: number
  height?: number
}

export type Margin = {
  top?: number
  right?: number
  bottom?: number
  left?: number
}

export type ChartProps<
  TDatum,
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
> = {
  title: string
  parentSelector: string
  hScaleStrategy: scale.ScaleStrategy<TDatum, THScale>
  vScaleStrategy: scale.ScaleStrategy<TDatum, TVScale>
  enterStrategyFactory: EnterStrategyFactory<TDatum, THScale, TVScale>
  data: TDatum[]
  dimension?: Dimension
  margin?: Margin
  axesMargin?: Margin
  bleed?: number
}

export type Chart<TDatum> = d3.Selection<
  SVGSVGElement,
  TDatum,
  HTMLElement,
  unknown
>

type ParentSelection<TParent extends SVGElement, TDatum> = d3.Selection<
  TParent,
  TDatum,
  HTMLElement,
  unknown
>

function appendTitle<TParent extends SVGElement, TDatum>(
  title: string,
): (parent: ParentSelection<TParent, TDatum>) => void {
  return function append(parent: ParentSelection<TParent, TDatum>) {
    parent
      .append("text")
      .attr("class", "chart__title")
      .text(title)
      .attr("x", 1)
      .attr("y", "0.5rem")
      .attr("dominant-baseline", "hanging")
  }
}

type GridLinesProps<
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
> = {
  hScale: THScale
  vScale: TVScale
  x: number
  y: number
  bleed?: number
}

function appendGridLines<TDatum>({
  type,
  g,
  x,
  y,
  inlineStart,
  inlineEnd,
  tickRanges,
}: {
  type: "h" | "v"
  g: d3.Selection<SVGGElement, TDatum, HTMLElement, unknown>
  x: number
  y: number
  inlineStart: number
  inlineEnd: number
  tickRanges: number[]
}) {
  const [inlineAxis, blockAxis] = type === "h" ? ["x", "y"] : ["y", "x"]
  g.attr("class", `chart__grid chart__grid--${type}`).attr(
    "transform",
    `translate(${x}, ${y})`,
  )
  tickRanges.forEach(function appendLine(tick) {
    g.append("line")
      .attr("class", `chart__grid-line chart__grid-line--${type}`)
      .attr(`${inlineAxis}1`, inlineStart)
      .attr(`${inlineAxis}2`, inlineEnd)
      .attr(`${blockAxis}1`, tick)
      .attr(`${blockAxis}2`, tick)
  })
}

function appendHGridLines<
  TParent extends SVGElement,
  TDatum,
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
>({
  hScale,
  vScale,
  x,
  y,
  bleed = 0,
}: GridLinesProps<THScale, TVScale>): (
  parent: ParentSelection<TParent, TDatum>,
) => void {
  return function append(parent: ParentSelection<TParent, TDatum>) {
    const [hRangeEnd] = hScale.range().slice(-1)
    const ticks = vScale.ticks()
    appendGridLines({
      type: "h",
      g: parent.append("g"),
      x,
      y,
      inlineStart: -bleed,
      inlineEnd: hRangeEnd,
      tickRanges: ticks.map((tick) => vScale(tick)),
    })
  }
}

function appendVGridLines<
  TParent extends SVGElement,
  TDatum,
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
>({
  hScale,
  vScale,
  x,
  y,
  bleed = 0,
}: GridLinesProps<THScale, TVScale>): (
  parent: ParentSelection<TParent, TDatum>,
) => void {
  return function append(parent: ParentSelection<TParent, TDatum>) {
    const [vRangeStart] = vScale.range()
    const ticks = hScale.ticks()
    appendGridLines({
      type: "v",
      g: parent.append("g"),
      x,
      y,
      inlineStart: -bleed,
      inlineEnd: vRangeStart,
      tickRanges: ticks.map((tick) => hScale(tick)),
    })
  }
}

type RectProps = {
  cy?: number
  cx?: number
  width: number | string
  height: number | string
  className?: string
}

function appendRect<TParent extends SVGElement, TDatum>({
  cx = 0,
  cy = 0,
  width,
  height,
  className,
}: RectProps): (
  parent: d3.Selection<TParent, TDatum, HTMLElement, unknown>,
) => void {
  return function append(
    parent: d3.Selection<TParent, TDatum, HTMLElement, unknown>,
  ) {
    const rect = parent
      .append("rect")
      .attr("width", width)
      .attr("height", height)
      .attr("transform", `translate(${cx}, ${cy})`)
    if (className !== undefined) rect.attr("class", className)
  }
}

export function chart<
  TDatum,
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
>({
  title,
  parentSelector,
  hScaleStrategy,
  vScaleStrategy,
  enterStrategyFactory,
  data,
  dimension = {},
  margin = {},
  axesMargin = {},
  bleed = 4,
}: ChartProps<TDatum, THScale, TVScale>): Chart<TDatum> {
  const { width = 800, height = 600 } = dimension
  const {
    top: marginTop = 20,
    right: marginRight = 20,
    bottom: marginBottom = 20,
    left: marginLeft = 20,
  } = margin
  const {
    top: axisMarginTop = 60,
    right: axisMarginRight = 40,
    bottom: axisMarginBottom = 20,
    left: axisMarginLeft = 10,
  } = axesMargin

  const innerWidth = width - marginLeft - marginRight
  const innerHeight = height - marginTop - marginBottom
  const hScale = hScaleStrategy(data, [
    0,
    innerWidth - axisMarginLeft - axisMarginRight,
  ])
  const vScale = vScaleStrategy(data, [
    innerHeight - axisMarginTop - axisMarginBottom,
    0,
  ])
  const enterStrategy = enterStrategyFactory({
    hScale,
    vScale,
  })

  const svgNode = d3
    .select<SVGSVGElement, TDatum>(parentSelector)
    .append("svg")
    .attr("width", width)
    .attr("height", height)
  svgNode
    .call(
      appendRect({
        width: "100%",
        height: "100%",
        className: "chart__background",
      }),
    )
    .append("g")
    .attr("class", "chart__content")
    .attr("transform", `translate(${marginLeft}, ${marginTop})`)
    .call(appendTitle(title))
    .call(
      appendRect({
        cx: axisMarginLeft - bleed,
        cy: axisMarginTop - bleed,
        width: innerWidth - axisMarginLeft - axisMarginRight + bleed,
        height: innerHeight - axisMarginTop - axisMarginBottom + bleed,
        className: "chart__series-background",
      }),
    )
    .call((g) =>
      g.select(".chart__series-background").on("click", (e) => {
        const [x, y] = d3.pointer(e)
        const [date, price] = [hScale.invert(x), vScale.invert(y)]
        // eslint-disable-next-line no-console
        console.log([x, y], [date, price])
      }),
    )
    .call(
      axis.appendBottomAxis({
        inlineScale: hScale,
        blockSize: innerHeight,
        marginBlockEnd: axisMarginBottom,
        marginInlineStart: axisMarginLeft,
      }),
    )
    .call(
      axis.appendRightAxis({
        inlineScale: vScale,
        blockSize: innerWidth,
        marginBlockEnd: axisMarginRight,
        marginInlineStart: axisMarginTop,
      }),
    )
    .call(
      appendHGridLines({
        hScale,
        vScale,
        x: axisMarginLeft,
        y: axisMarginTop,
        bleed,
      }),
    )
    .call(
      appendVGridLines({
        hScale,
        vScale,
        x: axisMarginLeft,
        y: axisMarginTop,
        bleed,
      }),
    )
    .append("g")
    .attr("class", "chart__series")
    .attr("transform", `translate(${axisMarginLeft}, ${axisMarginTop})`)
    .selectAll("g")
    .data(data)
    .join(enterStrategy)
  return svgNode
}
