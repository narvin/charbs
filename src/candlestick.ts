import * as d3 from "d3"
import * as chart from "./chart"
import * as scale from "./scale"

export type Candlestick = {
  date: Date
  open: number
  close: number
  low: number
  high: number
}

export const dateScaleStrategy: scale.ScaleStrategy<
  Candlestick,
  d3.ScaleTime<number, number>
> = (candles, range) =>
  scale.packedDateScale({
    dates: [
      ...candles.map((c) => c.date),
      d3.timeDay.offset(candles[candles.length - 1].date, 1),
    ],
    interval: d3.utcDay,
    range,
    utc: true,
  })

export const priceScaleStrategy: scale.ScaleStrategy<
  Candlestick,
  d3.ScaleLinear<number, number>
> = (candles, range) => {
  const minPrice = d3.min(candles, (candle) => candle.low) || 1
  const maxPrice = d3.max(candles, (candle) => candle.high) || 1
  const priceRange = maxPrice - minPrice
  const pricePadding = 0.05 * priceRange
  return d3.scaleLinear(
    [minPrice - pricePadding, maxPrice + pricePadding],
    range,
  )
}

/**
 * Determine whether a candlestick is down, up, or neutral.
 *
 * @param args.open - the opening price of the candlestick.
 * @param args.close - the closing price of the candlestick.
 * @returns "down", "up", or "none" if the candlestick is down, up, or neutral.
 */
function getDirection({ open, close }: { open: number; close: number }) {
  if (open > close) return "down"
  if (open < close) return "up"
  return "none"
}

/**
 * Sort the open and close from low to high.
 *
 * @param open - the opening price of the candlestick.
 * @param close - the closing price of the candlestick.
 * @returns "down", "up", or "none" if the candlestick is down, up, or neutral.
 */
function getBodyExtrema(open: number, close: number) {
  return open < close ? [open, close] : [close, open]
}

/**
 * Properties to create a candlestick body sized and positioned along a vertical scale.
 */
type BoxProps = {
  /** The left edge of the body. */
  x: number
  /** The width of the candlestick body. */
  width: number
  /** The {@link d3.ScaleLinear} to use to size and position the candlestick vertically. */
  yScale: d3.ScaleLinear<number, number>
  /** The width of the candlestick body. */
}

/**
 * Create a function that can be called with a {@link d3.Selection} to append a candlestick
 * body to.
 *
 * @param props - {@link BoxProps}
 * @returns A function that will append a candlestick body to a selection. This function can
 *   be used as the argument to {@link d3.Selection.call}.
 */
function appendBox({ x, width, yScale }: BoxProps) {
  return function append(
    g: d3.Selection<SVGGElement, Candlestick, SVGGElement, Candlestick>,
  ) {
    g.append("rect")
      .attr("class", (candle) => {
        const { open, close } = candle
        return `candlestick__body candlestick__body--${getDirection({
          open,
          close,
        })}`
      })
      .attr("width", width)
      .attr("height", (candle) => {
        const { open, close } = candle
        return Math.max(Math.abs(yScale(open) - yScale(close)), 0.5)
      })
      .attr("transform", (candle) => {
        const { open, close } = candle
        const [_, bodyHigh] = getBodyExtrema(open, close)
        return `translate(${x}, ${yScale(bodyHigh)})`
      })
  }
}

/**
 * Properties to create a candlestick tail sized and positioned along a vertical scale.
 */
type TailProps = {
  /** The type of tail. */
  type: "bottom" | "top"
  /** The center of the body. */
  x: number
  /** The {@link d3.ScaleLinear} to use to size and position the candlestick vertically. */
  yScale: d3.ScaleLinear<number, number>
}

/**
 * Create a function that can be called with a {@link d3.Selection} to append a candlestick
 * tail to.
 *
 * @param props - {@link TailProps}
 * @returns A function that will append a candlestick tail to a selection. This function can
 *   be used as the argument to {@link d3.Selection.call}.
 */
function appendTail({ type, x, yScale }: TailProps) {
  return function append(
    g: d3.Selection<SVGGElement, Candlestick, SVGGElement, Candlestick>,
  ) {
    g.append("line")
      .attr("class", (candle) => {
        const { open, close } = candle
        return `candlestick__tail candlestick__tail--${getDirection({
          open,
          close,
        })}`
      })
      .attr("x1", x)
      .attr("y1", (candle) => {
        const { open, close } = candle
        const [bodyLow, bodyHigh] = getBodyExtrema(open, close)
        const y1 = type === "bottom" ? bodyLow : bodyHigh
        return yScale(y1)
      })
      .attr("x2", x)
      .attr("y2", (candle) => {
        const { low, high } = candle
        const y2 = type === "bottom" ? low : high
        return yScale(y2)
      })
  }
}

/**
 * Create a {@link chart.EnterStrategy} function that will create a function to append
 * candlesticks to an enter selection.
 *
 * @param props.padding - The total padding on both sides of a candlestick. The total padding
 *   will be capped at half the distance between the centers of two adjacent candlesticks.
 * @returns A function that will use the given scales, and the enter selection's data to
 *   append, and position candlesticks.
 */
export function createCandlestickEnterStrategyFactory({
  padding = 8,
}: {
  padding?: number
} = {}): chart.EnterStrategyFactory<
  Candlestick,
  d3.ScaleTime<number, number>,
  d3.ScaleLinear<number, number>
> {
  return function candlestickEnterStrategyFactory({
    hScale,
    vScale,
  }: chart.EnterStrategyFactoryProps<
    d3.ScaleTime<number, number>,
    d3.ScaleLinear<number, number>
  >): chart.EnterStrategy<Candlestick> {
    const [rangeStart, rangeEnd] = [
      ...hScale.range().slice(0, 1),
      ...hScale.range().slice(-1),
    ]
    return function strategy(
      enter: d3.Selection<
        d3.EnterElement,
        Candlestick,
        SVGGElement,
        Candlestick
      >,
    ) {
      const step = (rangeEnd - rangeStart) / enter.data().length
      const width = step - Math.min(padding, step * 0.5)
      const bodyX = (step - width) / 2
      const tailX = bodyX + width / 2
      return enter
        .append("g")
        .attr("class", "candlestick")
        .call(appendTail({ type: "bottom", x: tailX, yScale: vScale }))
        .call(
          appendTail({
            type: "top",
            x: tailX,
            yScale: vScale,
          }),
        )
        .call(appendBox({ x: bodyX, width, yScale: vScale }))
        .attr("transform", (candle) => `translate(${hScale(candle.date)}, 0)`)
    }
  }
}
