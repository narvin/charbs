import { Mock, beforeEach, describe, expect, test, vi } from "vitest"
import * as d3 from "d3"
import * as chart from "./chart"
import * as scale from "./scale"

const d3Mock = vi.hoisted(function magicMock() {
  const mock = vi.fn()
  mock.mockReturnValue(mock)
  const proxy: Mock = new Proxy(mock, {
    get(target, prop) {
      if (prop === "mock") return target.mock
      if (prop === "mockClear") return target.mockClear
      return proxy
    },
    apply(target, _thisArg, args) {
      target(...args)
      return proxy
    },
  })
  return proxy
})

vi.mock("d3", async function mod() {
  const actual = await vi.importActual<typeof import("d3")>("d3")
  return {
    ...actual,
    select: d3Mock,
  }
})

function enterStrategyFactory<
  THScale extends scale.Scale,
  TVScale extends scale.Scale,
>({
  hScale: _hScale,
  vScale: _vScale,
}: chart.EnterStrategyFactoryProps<
  THScale,
  TVScale
>): chart.EnterStrategy<never> {
  return function strategy(parent) {
    return parent.append("g")
  }
}

describe("chart", () => {
  beforeEach(() => {
    d3Mock.mockClear()
  })
  test("uses default dimension", () => {
    chart.chart({
      title: "foo",
      parentSelector: "#app",
      hScaleStrategy: (_values, _range) => d3.scaleUtc(),
      vScaleStrategy: (_values, _range) => d3.scaleLinear(),
      enterStrategyFactory,
      data: [],
    })
    expect(d3Mock.mock.calls.slice(0, 4)).toEqual([
      ["#app"],
      ["svg"],
      ["width", 800],
      ["height", 600],
    ])
  })
  test("uses passed dimension width", () => {
    chart.chart({
      title: "foo",
      parentSelector: "#app",
      hScaleStrategy: (_values, _range) => d3.scaleUtc(),
      vScaleStrategy: (_values, _range) => d3.scaleLinear(),
      enterStrategyFactory,
      data: [],
      dimension: { width: 400 },
    })
    expect(d3Mock.mock.calls.slice(0, 4)).toEqual([
      ["#app"],
      ["svg"],
      ["width", 400],
      ["height", 600],
    ])
  })
  test("uses passed dimension height", () => {
    chart.chart({
      title: "foo",
      parentSelector: "#app",
      hScaleStrategy: (_values, _range) => d3.scaleUtc(),
      vScaleStrategy: (_values, _range) => d3.scaleLinear(),
      enterStrategyFactory,
      data: [],
      dimension: { height: 400 },
    })
    expect(d3Mock.mock.calls.slice(0, 4)).toEqual([
      ["#app"],
      ["svg"],
      ["width", 800],
      ["height", 400],
    ])
  })
  test("uses passed dimension", () => {
    chart.chart({
      title: "foo",
      parentSelector: "#app",
      hScaleStrategy: (_values, _range) => d3.scaleUtc(),
      vScaleStrategy: (_values, _range) => d3.scaleLinear(),
      enterStrategyFactory,
      data: [],
      dimension: { width: 400, height: 200 },
    })
    expect(d3Mock.mock.calls.slice(0, 4)).toEqual([
      ["#app"],
      ["svg"],
      ["width", 400],
      ["height", 200],
    ])
  })
})
